<?php

function news_calendar_settings_form($form, $form_state) {
  $node_types = node_type_get_names();
  $selected_types = variable_get('news_calendar_types', array());
  $form['types'] = array(
    '#type' => 'select',
    '#title' => t('Select node types'),
    '#multiple' => TRUE,
    '#options' => $node_types,
    '#required' => TRUE,
    '#default_value' => $selected_types
  );
  $form['date_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Date pattern in url part'),
    '#default_value' => news_calendar_get_date_pattern(),
    '#required' => TRUE,
    '#description' => t('Use placeholders %d - day, %m - month, %y - year'),
  );
  if ($selected_types) {
    $form['types_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Calendar path patterns'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );
    foreach ($selected_types as $type) {
      $form['types_settings'][$type] = array(
        '#type' => 'textfield',
        '#default_value' => news_calendar_get_type_pattern($type),
        '#title' => t('Path pattern for nodes %type', array('%type' => $node_types[$type])),
        '#description' => t('Set path pattern, use date placeholder !date. If you want to use GET attribute type "?" syumbol.')
      );
    }
  }
  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}

function news_calendar_settings_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $selected_types = $values['types'];
  variable_set('news_calendar_types', $selected_types);
  variable_set('news_calendar_date_pattern', $values['date_pattern']);
  $type_patterns = array();
  if (!empty($values['types_settings'])) {
    foreach ($values['types_settings'] as $type => $pattern) {
      $type_patterns[$type] = $pattern;
    }
  }
  variable_set('news_calendar_patterns', $type_patterns);
}
